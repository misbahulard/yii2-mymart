<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$formatter = \Yii::$app->formatter;

$this->title = 'Show Order';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php foreach ($model as $order) { 
    $x = 1;
    $totalPrice = 0;
?>
    <h4><?= 'Order number: ' . $order->id ?></h4>
    <small><?= $formatter->asDatetime($order->date) ?></small>
    
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($order->items as $item) { ?>
            <tr>
                <td><?= $x ?></td>
                <td><?= $item->name ?></td>
                <td><?= $formatter->format($item->price, ['currency', 'Rp.']) ?></td>
            </tr>
        <?php
            $totalPrice += $item->price; 
            $x++;
            } 
        ?>
            <tr>
                <td></td>
                <td>Total</td>
                <td><?= $formatter->format($totalPrice, ['currency', 'Rp.']) ?></td>
            </tr>
        </tbody>
    </table>
<?php } ?>