<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Order';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">
    <h1>Your Order</h1>

    <p>
        <?= Html::a('Checkout', ['checkout'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Clear', ['clear'], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'price'
            ],
    ]); ?>
</div>
