<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="order-checkout">    
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Thank You!</strong> for shopping in My-mart
    </div>
    <?= Html::a('Back', ['/'], ['class' => 'btn btn-success']) ?>
</div>
