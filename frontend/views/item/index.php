<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* Author Misbahul Ardani */

$this->title = 'Product';
Yii::$app->language = 'id-ID';
?>

<style>
    .item-box {
        border-radius: 3px;
        border: 1px solid #e0e0e0;
        padding-top: 15px;
        padding-left: 15px;
        padding-right: 15px;
        padding-bottom: 5px;
        margin-bottom: 15px;
    }

    .item-box:hover {
        border: 1px solid #cccccc;
        box-shadow: 3px 2px 5px #e7e7e7;
    }

    .item-box-link {
        color: #000;
    }

    .item-box-link:hover {
        text-decoration: none;
        color: #0042bd;
    }

    .img-container {
        border-radius: 3px;
        margin: auto;
        position: relative;
        overflow: hidden;
        width: 200px;
        height: 200px;
    }

    .crop {
        position: absolute;
        left: -100%;
        right: -100%;
        top: -100%;
        bottom: -100%;
        margin: auto;
        width: 100%;
        height: auto;
    }
</style>

<div class="item-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <p>Browse by category:</p>
            <div class="dropdown pull-left">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                    Category
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <?php foreach ($categories as $category): ?>
                        <li>
                            <a href="<?= Url::base() . '/item/index?category_id=' . $category->id ?>"><?= $category->name ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php if (Yii::$app->request->queryString != '') { ?>
                <a class="btn btn-danger" href="<?= Url::base() . '/item' ?>">X</a>
            <?php } ?>
        </div>
    </div>
    <hr>

    <div class="row">

        <?php foreach ($models as $model): ?>
            <div class="col-xs-6 col-md-3">
                <a class="item-box-link" href="<?= Url::base() . '/item/view?id=' . $model->id ?>">
                    <div class="item-box">

                        <div class="img-container">
                            <img src="<?= Yii::$app->urlManagerBackend->createUrl('') . 'uploads/' . $model->image ?>"
                                 alt="<?= $model->name ?>" class="crop">
                        </div>
                        <hr>
                        <h4><?= $model->name ?></h4>
                        <h5><?= Yii::$app->formatter->asCurrency($model->price) ?></h5>
                    </div>
                </a>
            </div>
        <?php endforeach ?>

    </div>

    <div class="row">
        <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
    </div>
</div>
