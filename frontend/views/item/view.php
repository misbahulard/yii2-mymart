<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Item */
/* Author Misbahul Ardani */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        if (Yii::$app->user->isGuest) {
            echo '<p>';
                echo '<div class="alert alert-danger" role="alert">Before buy an item, you must login first!</div>';
                echo Html::a('Login First', ['/site/login'], ['class' => 'btn btn-danger']);
            echo '</p>';
        } else {
            if ($customer) {
                echo '<p>';
                    echo Html::a('Add to cart', ['addtocart', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo '</p>';    
            } else {
                echo '<p>';
                    echo '<div class="alert alert-danger" role="alert">Before buy an item, create your identity first!</div>';
                    echo Html::a('Create Customer', ['/customer/create', 'id' => $model->id], ['class' => 'btn btn-danger']);
                echo '</p>';    
            }
        } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'image',
                'value' => Yii::$app->urlManagerBackend->createUrl('') . 'uploads/' . $model->image,
                'format' => ['image', ['width' => '100px']]
            ],
            'id',
            'name',
            [
                'attribute' => 'price',
                'format' => ['currency', 'Rp.']
            ],
            'category_id',
        ],
    ]) ?>

</div>
