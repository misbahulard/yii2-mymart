<?php

namespace frontend\components; 
use Yii; 
use yii\base\component;
use frontend\models\Statistic; 

/**
 * Class for record statistic
 */
class StatisticRecord extends component
{
    const EVENT_RECORD_STATISTIC = 'statistic-record';

    public function statisticHandler(){
        $statistic = [
            'access_time' => date('Y-m-d H:i:s'),
            'user_ip' => Yii::$app->request->userIP,
            'user_host' => Yii::$app->request->userHost,
            'path_info' => Yii::$app->request->pathInfo, 
            'query_string' => Yii::$app->request->queryString
        ];    
        
        $model = new Statistic();

        $model->addStatistic($statistic);
    }
}