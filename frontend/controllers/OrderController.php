<?php

namespace frontend\controllers;

use Yii;
use common\models\Customer;
use frontend\models\Order;
use frontend\models\OrderItem;
use frontend\models\Item;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Cookie;
use yii\data\ActiveDataProvider;

class OrderController extends \yii\web\Controller
{

    public function actionIndex()
    {
        
        $cartlist = $this->getCartList();
        
        $orderlist = Item::find()->where(['id' => $cartlist]);

        $dataProvider = new ActiveDataProvider([
            'query' => $orderlist
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckout()
    {
        $cartlist = $this->getCartList();

        $transaction = Order::getDb()->beginTransaction();
        try {
            $customer = Customer::findOne([
                'user_id' => Yii::$app->user->identity->id
            ]);
            $order = new Order();
            $order->date = date("Y-m-d H:i:s");
            $order->customer_id = $customer->id;
            $order->save();
            
            $orderId = Order::find([
                'customer_id' => $customer->id
            ])->orderBy('id DESC')->one();

            for ($i=0; $i < sizeof($cartlist); $i++) { 
                $orderItem = new OrderItem();
                $orderItem->order_id = $orderId->id;
                $orderItem->item_id = $cartlist[$i];
                $orderItem->save();
            }
            $transaction->commit();
            $this->removeCartList();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->render('checkout');
    }
    
    public function actionClear()
    {
        $this->removeCartList();
        $this->redirect(['order/index']);
    }

    public function getCartList()
    {
        $count = 0;
        $cartlist = [];
        
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('totalcart')) {
            $count = $cookies->getValue('totalcart');
            for ($i=0; $i <= $count; $i++) { 
                array_push($cartlist, $cookies->getValue('cart'.$i));
            }
        }

        return $cartlist;
    }

    public function removeCartList()
    {
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('totalcart')) {
            $count = $cookies->getValue('totalcart');
            $cookies = Yii::$app->response->cookies;
            for ($i=0; $i <= $count; $i++) { 
                $cookies->remove('cart'.$i);
            }
            $cookies->remove('totalcart');
        }
    }

}
