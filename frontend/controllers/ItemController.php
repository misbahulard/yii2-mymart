<?php

namespace frontend\controllers;

use frontend\models\ItemCategory;
use Yii;
use frontend\models\Item;
use frontend\models\ItemSearch;
use frontend\models\Statistic;
use frontend\models\Customer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Cookie;
/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex($category_id = null)
    {
        Yii::$app->statisticRecord->trigger(\frontend\components\StatisticRecord::EVENT_RECORD_STATISTIC);

        if ($category_id != null) {
            $query = Item::find()->where(['category_id' => $category_id]);
        } else {
            $query = Item::find();
        }
        $pages = new \yii\data\Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 8
        ]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();
        $categories = ItemCategory::find()->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'categories' => $categories
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        /**
        * For checing if customer was created before
        * So User will get notification when customer doesn't exist
        */
        if (Yii::$app->user->isGuest) {
            $customer = false;
        } else {
            $customer = Customer::findOne([
                'user_id' => Yii::$app->user->identity->id
            ]);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'customer' => $customer
        ]);
    }
    
    public function actionAddtocart($id)
    {
        $count = 0;
        // get the cookie collection (yii\web\CookieCollection) from the "response" component
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('totalcart')) {
            $count = $cookies->getValue('totalcart');
            $count++;;
            // get the cookie collection (yii\web\CookieCollection) from the "response" component
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new Cookie([
                'name' => 'totalcart',
                'value' => $count,
            ]));
        } else {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new Cookie([
                'name' => 'totalcart',
                'value' => 0,
            ]));    
        }

        $cookies = Yii::$app->response->cookies;
        // set username
        if (!$cookies->has('username')){
            $cookies->add(new Cookie([
                'name' => 'username',
                'value' => Yii::$app->user->identity->username,
            ]));    
        }

        // add a new cookie to the response to be sent
        $cookies->add(new Cookie([
            'name' => 'cart'.$count,
            'value' => $id,
        ]));

        return $this->redirect('index');

    }

     /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
