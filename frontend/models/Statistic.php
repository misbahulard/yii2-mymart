<?php

namespace frontend\models;

use Yii;
/* Author Misbahul Ardani */
/**
 * This is the model class for table "statistic".
 *
 * @property integer $id
 * @property string $access_time
 * @property string $user_ip
 * @property string $user_host
 * @property string $path_info
 * @property string $query_string
 */
class Statistic extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_time', 'user_ip', 'user_host', 'path_info', 'query_string'], 'required'],
            [['access_time'], 'safe'],
            [['user_ip'], 'string', 'max' => 20],
            [['user_host', 'path_info', 'query_string'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_time' => 'Access Time',
            'user_ip' => 'User Ip',
            'user_host' => 'User Host',
            'path_info' => 'Path Info',
            'query_string' => 'Query String',
        ];
    }

    public function addStatistic($statistic)
    {
        if ($statistic['user_host'] == NULL && $statistic['query_string'] == "") {
            $statistic['user_host'] = "-";
            $statistic['query_string'] = "-";
        };

        $this->access_time = $statistic['access_time'];
        $this->user_ip = $statistic['user_ip'];
        $this->user_host = $statistic['user_host'];
        $this->path_info = $statistic['path_info'];
        $this->query_string = $statistic['query_string'];
    
        $this->save();
    }
}
