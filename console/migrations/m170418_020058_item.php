<?php

use yii\db\Migration;

class m170418_020058_item extends Migration
{
    public function up()
    {
        $this->createTable('item', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'price' => $this->int(),
        ]);
    }

    public function down()
    {
        $this->dropTable('item');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
