-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `item_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item` (`id`, `name`, `price`, `category_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3,	'Ayat cinta',	76000,	100,	1491879803,	1491879803,	2,	2),
(4,	'Hans Denim',	310000,	101,	1492405712,	1492478266,	2,	3),
(5,	'JS Ninja',	70000,	100,	1492478252,	1492478252,	3,	3),
(6,	'PSD',	230000,	101,	1492478287,	1492478287,	3,	3),
(7,	'Buku Baru',	99000,	100,	1492480034,	1492480034,	3,	3);

DROP TABLE IF EXISTS `item_category`;
CREATE TABLE `item_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item_category` (`id`, `name`, `parent_category`) VALUES
(100,	'buku',	1),
(101,	'kemeja',	2);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1490141506),
('m130524_201442_init',	1490141509);

DROP TABLE IF EXISTS `statistic`;
CREATE TABLE `statistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_time` datetime DEFAULT NULL,
  `user_ip` varchar(20) DEFAULT NULL,
  `user_host` varchar(50) DEFAULT NULL,
  `path_info` varchar(50) DEFAULT NULL,
  `query_string` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `statistic` (`id`, `access_time`, `user_ip`, `user_host`, `path_info`, `query_string`) VALUES
(19,	'2017-04-04 06:16:03',	'::1',	'-',	'site/about',	'-'),
(20,	'2017-04-04 06:16:19',	'::1',	'-',	'site/about',	'-'),
(21,	'2017-04-04 06:16:30',	'::1',	'-',	'site/about',	'-'),
(22,	'2017-04-11 04:05:04',	'::1',	'-',	'site/signup',	'-'),
(23,	'2017-04-11 04:05:23',	'::1',	'-',	'site/signup',	'-'),
(24,	'2017-04-11 04:35:30',	'::1',	'-',	'site/index',	'-'),
(25,	'2017-04-11 04:35:41',	'::1',	'-',	'site/about',	'-'),
(26,	'2017-04-11 04:36:58',	'::1',	'-',	'site/about',	'-'),
(27,	'2017-04-11 04:37:05',	'::1',	'-',	'site/about',	'-'),
(28,	'2017-04-11 04:38:56',	'::1',	'-',	'site/about',	'-'),
(29,	'2017-04-11 04:39:06',	'::1',	'-',	'site/about',	'-'),
(30,	'2017-04-11 04:39:27',	'::1',	'-',	'site/about',	'-'),
(31,	'2017-04-11 04:45:13',	'::1',	'-',	'site/about',	'-'),
(32,	'2017-04-11 04:49:11',	'::1',	'-',	'site/contact',	'-'),
(33,	'2017-04-11 04:51:13',	'::1',	'-',	'site/signup',	'-'),
(34,	'2017-04-18 03:16:23',	'::1',	'-',	'site/about',	'-'),
(35,	'2017-04-18 03:16:29',	'::1',	'-',	'site/signup',	'-'),
(36,	'2017-04-18 03:16:45',	'::1',	'-',	'site/signup',	'-'),
(37,	'2017-04-18 03:16:50',	'::1',	'-',	'site/logout',	'-'),
(38,	'2017-04-18 03:19:22',	'::1',	'-',	'site/index',	'-'),
(39,	'2017-04-18 03:46:40',	'::1',	'-',	'site/about',	'-'),
(40,	'2017-04-18 03:54:23',	'::1',	'-',	'item/index',	'-');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(2,	'misbahulard',	'_o5SN7F3604AKm0qi4_ODXPCLDOBcjTf',	'$2y$13$7n/vhXT5TVSNMNaFu6hR3Oe0delnWzTAeYE/VY72a.RKjD6xyUh8S',	NULL,	'misbahulard@gmail.com',	10,	1491876324,	1491876324),
(3,	'ardani',	'Q56uIhUG7CwJMsL6FL2DIEc9cTdcEw-V',	'$2y$13$UP6wdjTaAo3Xzv/96Fb2K.96Zd5FAjiJcaQ6na3yjVLhdPBQZNFte',	NULL,	'ardani@gmail.com',	10,	1492478206,	1492478206);

-- 2017-04-18 02:09:43-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `item_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `item_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item` (`id`, `name`, `price`, `category_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3,	'Ayat cinta',	76000,	100,	1491879803,	1491879803,	2,	2),
(4,	'Hans Denim',	310000,	101,	1492405712,	1492478266,	2,	3),
(5,	'JS Ninja',	70000,	100,	1492478252,	1492478252,	3,	3),
(6,	'PSD',	230000,	101,	1492478287,	1492478287,	3,	3),
(7,	'Buku Baru',	99000,	100,	1492480034,	1492480034,	3,	3);

DROP TABLE IF EXISTS `item_category`;
CREATE TABLE `item_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item_category` (`id`, `name`, `parent_category`) VALUES
(100,	'buku',	1),
(101,	'kemeja',	2);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1490141506),
('m130524_201442_init',	1490141509);

DROP TABLE IF EXISTS `statistic`;
CREATE TABLE `statistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_time` datetime DEFAULT NULL,
  `user_ip` varchar(20) DEFAULT NULL,
  `user_host` varchar(50) DEFAULT NULL,
  `path_info` varchar(50) DEFAULT NULL,
  `query_string` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `statistic` (`id`, `access_time`, `user_ip`, `user_host`, `path_info`, `query_string`) VALUES
(19,	'2017-04-04 06:16:03',	'::1',	'-',	'site/about',	'-'),
(20,	'2017-04-04 06:16:19',	'::1',	'-',	'site/about',	'-'),
(21,	'2017-04-04 06:16:30',	'::1',	'-',	'site/about',	'-'),
(22,	'2017-04-11 04:05:04',	'::1',	'-',	'site/signup',	'-'),
(23,	'2017-04-11 04:05:23',	'::1',	'-',	'site/signup',	'-'),
(24,	'2017-04-11 04:35:30',	'::1',	'-',	'site/index',	'-'),
(25,	'2017-04-11 04:35:41',	'::1',	'-',	'site/about',	'-'),
(26,	'2017-04-11 04:36:58',	'::1',	'-',	'site/about',	'-'),
(27,	'2017-04-11 04:37:05',	'::1',	'-',	'site/about',	'-'),
(28,	'2017-04-11 04:38:56',	'::1',	'-',	'site/about',	'-'),
(29,	'2017-04-11 04:39:06',	'::1',	'-',	'site/about',	'-'),
(30,	'2017-04-11 04:39:27',	'::1',	'-',	'site/about',	'-'),
(31,	'2017-04-11 04:45:13',	'::1',	'-',	'site/about',	'-'),
(32,	'2017-04-11 04:49:11',	'::1',	'-',	'site/contact',	'-'),
(33,	'2017-04-11 04:51:13',	'::1',	'-',	'site/signup',	'-'),
(34,	'2017-04-18 03:16:23',	'::1',	'-',	'site/about',	'-'),
(35,	'2017-04-18 03:16:29',	'::1',	'-',	'site/signup',	'-'),
(36,	'2017-04-18 03:16:45',	'::1',	'-',	'site/signup',	'-'),
(37,	'2017-04-18 03:16:50',	'::1',	'-',	'site/logout',	'-'),
(38,	'2017-04-18 03:19:22',	'::1',	'-',	'site/index',	'-'),
(39,	'2017-04-18 03:46:40',	'::1',	'-',	'site/about',	'-'),
(40,	'2017-04-18 03:54:23',	'::1',	'-',	'item/index',	'-');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(2,	'misbahulard',	'_o5SN7F3604AKm0qi4_ODXPCLDOBcjTf',	'$2y$13$7n/vhXT5TVSNMNaFu6hR3Oe0delnWzTAeYE/VY72a.RKjD6xyUh8S',	NULL,	'misbahulard@gmail.com',	10,	1491876324,	1491876324),
(3,	'ardani',	'Q56uIhUG7CwJMsL6FL2DIEc9cTdcEw-V',	'$2y$13$UP6wdjTaAo3Xzv/96Fb2K.96Zd5FAjiJcaQ6na3yjVLhdPBQZNFte',	NULL,	'ardani@gmail.com',	10,	1492478206,	1492478206);

-- 2017-04-18 02:09:43